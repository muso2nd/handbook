#Handbook

This is a basic handbook of generic helpful tips and tricks.

Currently it contains:

##Excel & Libreoffice Calc

###VLookup, Index and Match
####Introduction
####Syntax of VLookup, Index and Match

###Index+Match
####Syntax
####Usage with examples
####Index+Match with horizontal search
####Index+Match+Match

